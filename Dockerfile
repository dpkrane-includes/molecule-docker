FROM python:3.10.10-slim-bullseye

LABEL vendor1="DeusOps"
LABEL vendor2="Konstantin Deepezh aka D3pRe5s"

ENV ANSIBLE_VERSION=7.3.0
ENV MOLECULE_VERSION=4.0.4
ENV ANSIBLELINT_VERSION=6.14.1
ENV MOLECULEDOCKER_VERSION=2.1.0
ENV YAMLLINT_VERSION=1.29.0

RUN apt-get update && apt-get install -y docker gcc musl-dev \
    && pip3 install --no-cache-dir --upgrade ansible~=${ANSIBLE_VERSION} \
    molecule~=${MOLECULE_VERSION} molecule-docker~=${MOLECULEDOCKER_VERSION} \
    yamllint~=${YAMLLINT_VERSION} ansible-lint~=${ANSIBLELINT_VERSION} \
    && apt-get clean autoclean && apt-get autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/ && python3 -m pip cache purge

COPY molecule.yml /usr/local/lib/python3.10/site-packages/molecule/cookiecutter/molecule/{{cookiecutter.role_name}}/{{cookiecutter.molecule_directory}}/{{cookiecutter.scenario_name}}

WORKDIR /opt
