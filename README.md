# Образ docker с установленным ansible и molecule для автоматизации тестирования ролей в CI.

dpkrane/molecule
749 mb

Основан на python:3.10.10-slim-bullseye.
В образ входят:
```
ANSIBLE 7.3.0
MOLECULE 4.0.4
ANSIBLE-LINT 6.14.1
MOLECULE-DOCKER 2.1.0
YAMLLINT 1.29.0
```
